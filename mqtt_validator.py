import time
import json
import psycopg2
import paho.mqtt.subscribe as subscribe
from cryptography.fernet import Fernet
from jsonschema import Draft7Validator
import logging

jtep = None
bat = None
key = "UOC-DMXptS_t7JPi_omDofSfTYaYVTqn638NLMl_zYE="
cipher_suite = Fernet(key)

#Statu's Schema
with open('SCHEMA_STATUS.json', 'r') as schema_status_obj:
    data=schema_status_obj.read()

SCHEMA_STATUS = json.loads(data)

#Device's Schema
with open('SCHEMA_DEVICE.json', 'r') as schema_device_obj:
    data=schema_device_obj.read()

SCHEMA_DEVICE = json.loads(data)


LOG_FORMAT = '[%(filename)s][%(funcName)s][%(asctime)s]: %(message)s'
logging.basicConfig(filename="./mqtt.log",
                    filemode='a',
                    format=LOG_FORMAT,
                    level=logging.DEBUG)
logger = logging.getLogger()


class ObjValidator():
    """ObjValidator validate the obj received with the SCHEMA"""
    def __init__(self):
        """__init__"""
        # self.server_validator = Draft7Validator(SCHEMA_SERVER)
        self.device_validator = Draft7Validator(SCHEMA_DEVICE)
        # self.alert_validator = Draft7Validator(SCHEMA_ALERT)
        self.status_validator = Draft7Validator(SCHEMA_STATUS)

    def is_valido(self, topic, obj):
        """
        The is_valido method validates the obj received
        and return a response after validate it.

        REQUIRED:
            topic <class 'str'>: The name of the topic to match it with a SCHEMA_X
            obj <class 'dict'>: The object to validate

        Return a list [is_valid, message]
        """
        self.topic = topic
        self.obj = obj

        if '/device' in self.topic:
            response=self.device_validator.is_valid(self.obj)
            if response:
                response = "Device Topic's Object Validation: Success\n"
                response = [True, response]
            else:
                response= "Device Topic's Object Validation: Error\n"
                count=0
                errors = sorted(self.device_validator.iter_errors(self.obj), key=lambda e: e.path)
                for error in errors:
                    response+='ERROR: {}\nHERE: {}\n'.format(error.message, error.path)
                    count+=1
                response+="TOTAL ERRORS: {}\n\n".format(count)
                response = [False, response]

        elif '/status' in self.topic:

            response=self.status_validator.is_valid(self.obj)
            if response:
                response = "Status Topic's Object Validation: Success\n"
                response = [True, response]
            else:
                response= "Status Topic's Object Validation: Error\n"
                count=0
                errors = sorted(self.status_validator.iter_errors(self.obj), key=lambda e: e.path)
                for error in errors:
                    response+='ERROR: {}\nHERE: {}\n'.format(error.message, error.path)
                    count+=1
                response+="TOTAL ERRORS: {}\n\n".format(count)
                response = [False, response]
                 

        return response


class DataBase():
    def __init__(self, db, user, password, host):
        self.conn = psycopg2.connect(
                                        host=host,
                                        dbname=db,
                                        user=user,
                                        password=password,
                                        )
        self.conn.autocommit = True
        self.cur = self.conn.cursor()

    def insert_message(self, msg):
        query = """INSERT INTO coordinates (latitude, longitude) VALUES ({}, {}) RETURNING id_coordinate;""".format(msg['devices']['gps']['latitude'], msg['devices']['gps']['longitude'])
        self.cur.execute(query)
        ret = self.cur.fetchone()[0]
        query = """INSERT INTO travels (vehicle_id,
                                            coordinate_id,
                                            create_event,
                                            create_record,
                                            speed,
					    synapse_identifier)
                                    VALUES ({}, {}, '{}', '{}',
                                            {},{});""".format(
                                        msg['sinapsis']['id'],
                                        ret,
                                        msg['message_details']['date'],
                                        time.strftime("%Y-%m-%d %H:%M:%S"),
                                        msg['devices']['gps']['speed'],
					msg['message_details']['id_message'])
        print(query)
        self.cur.execute(query)
        self.cur.execute("""UPDATE vehicles
                            SET speed = ({}),
                                last_report = ('{}'),
                                power = ({})
                            WHERE id_vehicle = ({});""".format(
                                                    str(msg['devices']['gps']["speed"]).split(".")[0],
                                                    msg['message_details']["date"],
                                                    True, msg['sinapsis']["id"]))


def on_message(client, userdata, message):
    msg = json.loads(cipher_suite.decrypt(message.payload))

    #Validating JTEP's Topics
    if 'jtep' in message.topic:

        if 'devices' in msg: #Topic: #/device
            result = validator.is_valido(topic=message.topic, obj=msg)
            if result[0]: #Success
                print(result[1])
                # jtep.insert_message(msg) #ACTION

            else: #Error
                print(result[1])
                logger.warning(result[1])

        elif 'status' in msg: #Topic: #/status
            result = validator.is_valido(topic=message.topic, obj=msg)
            if result[0]: #Success
                print(result[1])
                # jtep.insert_message(msg) #ACTION

            else: #Error
                print(result[1])
                logger.warning(r[1])



def main():
    # jtep = DataBase('URBO', 'postgres', 'U3b02018', 'localhost') #DB INSTANCE
    subscribe.callback(on_message, "vikua/jtep/#", hostname="sinapsis.vikua.com")




validator = ObjValidator()
if __name__ == "__main__":
    main()
