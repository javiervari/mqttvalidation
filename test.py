from jsonschema import Draft7Validator

SCHEMA = {
   'type':'object',
   'required':[
      'info_travel',
      'message_details',
      'devices',
      'sinapsis'
   ],
   'properties':{
      'info_travel':{
         'type':'object',
         'required':[
            'avg_speed',
            'accumulated_time',
            'traveled_distance',
            'start_coord',
            'start_datetime',
            'id_travel'
         ],
         'properties':{
            'avg_speed':{
               'type':'number'
            },
            'accumulated_time':{
               'type':'string'
            },
            'traveled_distance':{
               'type':'number'
            },
            'start_coord':{
               'type':'string'
            },
            'start_datetime':{
               'type':'string'
            },
            'id_travel':{
               'type':'number'
            }
         }
      },
      'message_details':{
         'type':'object',
         'required':[
            'date',
            'id_message'
         ],
         'properties':{
            'date':{
               'type':'string'
            },
            'id_message':{
               'type':'string'
            }
         }
      },
      'devices':{
         'type':'object',
         'required':[
            'distance',
            'counter',
            'buttons',
            'gps'
         ],
         'properties':{
            'distance':{
               'type':'object',
               'required':[
                  'distance_level_2',
                  'distance_level_1'
               ],
               'properties':{
                  'distance_level_2':{
                     'type':'string'
                  },
                  'distance_level_1':{
                     'type':'string'
                  }
               }
            },
            'counter':{
               'type':'object',
               'required':[
                  'entry',
                  'out'
               ],
               'properties':{
                  'entry':{
                     'type':'string'
                  },
                  'out':{
                     'type':'string'
                  }
               }
            },
            'buttons':{
               'type':'object',
               'required':[
                  'panico'
               ],
               'properties':{
                  'panico':{
                     'type':'number'
                  }
               }
            },
            'gps':{
               'type':'object',
               'required':[
                  'speed',
                  'motion_direction',
                  'longitude',
                  'latitude',
                  'climb',
                  'altitude'
               ],
               'properties':{
                  'speed':{
                     'type':'string'
                  },
                  'motion_direction':{
                     'type':'string'
                  },
                  'longitude':{
                     'type':'string'
                  },
                  'latitude':{
                     'type':'string'
                  },
                  'climb':{
                     'type':'string'
                  },
                  'altitude':{
                     'type':'string'
                  }
               }
            }
         }
      },
      'sinapsis':{
         'type':'object',
         'required':[
            'version',
            'id',
            'new_id'
         ],
         'properties':{
            'version':{
               'type':'string'
            },
            'id':{
               'type':'string'
            },
            'new_id':{
               'type':'string'
            }
         }
      }
   }
}

class ObjValidator():
    """docstring for ObjValidator"""
    def __init__(self):
        self.validator = Draft7Validator(SCHEMA)

    def is_valido(self, obj):
        response=self.validator.is_valid(obj)
        if response:
            response = "Object Validated Successfully"
        else:
            response=''
            count=0
            print("Invalid Object")
            errors = sorted(self.validator.iter_errors(obj), key=lambda e: e.path)
            for error in errors:
                response+='ERROR: {}\nHERE: {}\n\n'.format(error.message, error.path)
                count+=1
            response+="TOTAL ERRORS: {}".format(count)

        return response



def main():
    validator = ObjValidator()
    
    valid_obj = {
    "info_travel":{
      "avg_speed":1.938,
      "accumulated_time":"20:43:21",
      "traveled_distance":0.0,
      "start_coord":"(10.48830434,-66.964137434)",
      "start_datetime":"2019-07-08 19:24:06",
      "id_travel":85
   },
   "message_details":{
      "date":"2019-07-09 16:07:22.420676",
      "id_message":"162873"
   },
   "devices":{
      "distance":{
         "distance_level_2":"None",
         "distance_level_1":"None"
      },
      "counter":{
         "entry":"0",
         "out":"0"
      },
      "buttons":{
         "panico":0
      },
      "gps":{
         "speed":"0.0",
         "motion_direction":"0.0",
         "longitude":"-66.925754744",
         "latitude":"10.498477946",
         "climb":"0.0",
         "altitude":"905.447"
      }
   },
   "sinapsis":{
      "version":"2.0",
      "id":"26",
      "new_id":"B-B827-EB11-8820"
   }
}
    
    obj_2={'1':1}
    
    obj_3={1}
    
    obj_4={'info_travel':False,
    'message_details':123,
    'devices':12,
    'sinapsis':'sinapsis'}

    obj_5={'info_travel':{'a':False},
    'message_details':{'b':1.1},
    'devices':{'c':5000},
    'sinapsis':{'id':True}}

    obj_6={'info_travel': {'avg_speed': True, 'accumulated_time': False, 'traveled_distance': 0.0, 'start_coord': '(10.48830434,-66.964137434)', 'start_datetime': '2019-07-08 19:24:06', 'id_travel': 85}, 'message_details': {'date': '2019-07-09 16:07:22.420676', 'id_message': '162873'}, 'devices': {'distance': {'distance_level_2': 'None', 'distance_level_1': 'None'}, 'counter': {'entry': '0', 'out': '0'}, 'buttons': {'panico': 0}, 'gps': {'speed': '0.0', 'motion_direction': '0.0', 'longitude': '-66.925754744', 'latitude': '10.498477946', 'climb': '0.0', 'altitude': '905.447'}}, 'sinapsis': {'version': '2.0', 'id': '26', 'new_id': 'B-B827-EB11-8820'}}

    response = validator.is_valido(valid_obj) 
    # response = validator.is_valido(obj_2) 
    # response = validator.is_valido(obj_3) 
    # response = validator.is_valido(obj_4) 
    # response = validator.is_valido(obj_5) 
    # response = validator.is_valido(obj_6) 
    print(response)


if __name__ == '__main__':
    main()

